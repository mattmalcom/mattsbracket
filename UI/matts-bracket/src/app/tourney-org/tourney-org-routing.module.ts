import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTourneyComponent } from './create-tourney/create-tourney.component';

const routes: Routes = [
  {
    path:'create-tourney',
    component: CreateTourneyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TourneyOrgRoutingModule { }
