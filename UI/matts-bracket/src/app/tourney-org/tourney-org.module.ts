import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { TourneyOrgRoutingModule } from './tourney-org-routing.module';
import { CreateTourneyComponent } from './create-tourney/create-tourney.component';

@NgModule({
  declarations: [CreateTourneyComponent],
  imports: [CommonModule, TourneyOrgRoutingModule, MatSlideToggleModule],
})
export class TourneyOrgModule {}
