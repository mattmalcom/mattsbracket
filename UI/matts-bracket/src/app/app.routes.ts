import { Routes } from '@angular/router';
import { TourneyOrgModule } from './tourney-org/tourney-org.module';

export const routes: Routes = [
    {
        path: 'tourney-org',
        loadChildren: () => import('./tourney-org/tourney-org.module').then((m) => TourneyOrgModule)
    }
];
